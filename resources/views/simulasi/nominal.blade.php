<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/jpg" sizes="96x96" href="/assets/images/logo_sttb_sm.jpg">
    <title>Transfer | Simulasi</title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- page css -->
    <link href="/css/pages/error-pages.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border fix-sidebar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- Tab panes -->
                    <div class="card-body" style="padding-top:100px">
                        <center><h2>Transfer</h2></center>
                        <form method="POST" class="form-horizontal form-material">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-12 text-center">Masukkan Nominal</label>
                                <div class="col-md-12">
                                    <input maxlength="6" min="0" minlength="6" type="number" name="nominal" class="form-control form-control-line text-center"required >
                                </div>
                            </div>
                            @if (session('message'))
                                <div class="alert alert-{{ session('type') }}">
                                {!! session('message') !!}
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" style="width:100%">Lanjut</button>
                                    <p style="margin-top: 20px; text-align: center;"><a href="/simulasi/keluar" style="color: red">Keluar</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
    <footer class="footer">
        © 2018 Lena Elisa | STTB
    </footer>
    <script src="/assets/node_modules/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/assets/node_modules/bootstrap/js/popper.min.js"></script>
    <script src="/assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
    <!--Wave Effects -->
    <script src="/js/waves.js"></script>
</body>

</html>