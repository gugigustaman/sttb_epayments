@extends('layouts.app')

@section('title', 'Beranda')

@section('styles')
<link href="/css/pages/dashboard1.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
        <div class="col-md-7 align-self-center">
            <a href="https://wrappixel.com/templates/adminwrap/" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down"> Upgrade to Pro</a>
        </div>
    </div> -->
    <div class="row">
        <!-- Column -->
        <!-- <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <div>
                            <h5 class="card-title m-b-0">Grafik Pembayaran</h5>
                        </div>
                        <div class="ml-auto">
                            <ul class="list-inline text-center font-12">
                                <li><i class="fa fa-circle text-success"></i> SITE A</li>
                                <li><i class="fa fa-circle text-info"></i> SITE B</li>
                                <li><i class="fa fa-circle text-primary"></i> SITE C</li>
                            </ul>
                        </div>
                    </div>
                    <div class="" id="sales-chart" style="height: 355px;"></div>
                </div>
            </div>
        </div> -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Selamat datang, {{ Auth::user()->nama }}</h5>
                    <br />
                    <p>Anda berada di halaman beranda admin pada STTB e-Payments. Pada modul admin ini, Anda dapat mengatur daftar mahasiswa, jurusan, tahun angkatan, kelas, biaya hingga biaya untuk masing-masing kelasnya.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/dashboard1.js"></script>
@endsection
