@extends('layouts.app')

@section('title', 'Daftar Biaya')

@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/node_modules/datatables/datatables.min.css"/>
<link href="/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Biaya
                        <a href="/admin/biaya/create" class="btn btn-sm btn-info pull-right"><i class="fa fa-plus"></i> &nbsp; Tambah</a>
                    </h4>
                    <h6 class="card-subtitle">Biaya selama perkuliahan</code></h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table" id="index">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/assets/node_modules/datatables/datatables.min.js"></script>
<script src="/assets/node_modules/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#index').DataTable({
            // processing: true,
            serverSide: false,
            ajax: {
                url: '{!! url('/admin/biaya/dtIndex') !!}',
                type: 'POST'
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'nama', name: 'nama' },
                { data: 'id', name: 'id', className: 'text-center actions' ,
                    render: function ( data, type, row ) {
                        var actions = "<a href='/admin/biaya/"+data+"/edit' class='btn btn-warning btn-xs'><i class='fa fa-edit'></i></a>";
                        actions += " <button data-id='"+data+"' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i></button>";
                        return actions;
                    }
                }
            ]
        });

        $('body').on('click', '.delete', function() {
            $('#id').val($(this).attr('data-id'));
            $('#delete-form').attr('action', '/admin/biaya/' + $('#id').val());
            swal({
                title: "Apakah Anda yakin?",
                text: "Biaya tersebut akan dihapus dari sistem.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!!",
                closeOnConfirm: false
            },
            function(){
                $('#delete-form').submit();
            });
        });
    });
</script>
@endsection
