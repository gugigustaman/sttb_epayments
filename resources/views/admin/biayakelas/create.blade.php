@extends('layouts.app')

@section('title', 'Tambah Mahasiswa')

@section('styles')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Start Notification -->
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <!-- Tab panes -->
                <div class="card-body">
                    <h4 class="card-title">Tambah Mahasiswa</h4>
                    <h6 class="card-subtitle">Tambah mahasiswa baru ke sistem</h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <form action="/admin/mahasiswa" method="POST" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">NPM</label>
                                    <div class="col-md-12">
                                        <input name="npm" type="text" class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Nama</label>
                                    <div class="col-md-12">
                                        <input type="text" name="nama" class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input name="password" type="password" class="form-control" form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Masukkan Kembali Password</label>
                                    <div class="col-md-12">
                                        <input name="password2" type="password" class="form-control" form-control-line" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" name="email" class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Jurusan</label>
                                    <div class="col-sm-12">
                                        <select name="id_jurusan" class="form-control form-control-line" required>
                                            <option value="">Pilih Jurusan</option>
                                            @foreach ($jurusan as $j)
                                            <option value="{{ $j->id }}">{{ $j->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Tahun Angkatan</label>
                                    <div class="col-sm-12">
                                        <select name="id_tahun_angkatan" id="id_tahun_angkatan" class="form-control form-control-line" required>
                                            <option value="">Pilih Tahun Angkatan</option>
                                            @foreach ($tahun_ajaran as $t)
                                            <option value="{{ $t->id }}">{{ $t->tahun_ajaran }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Kelas</label>
                                    <div class="col-sm-12">
                                        <select name="id_kelas" id="id_kelas" class="form-control form-control-line" required>
                                            <option value="">Pilih Kelas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control form-control-line" required>
                                            <option value="0">Aktif</option>
                                            <option value="1">Non Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success" style="width: 100%">Tambah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Notification -->
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        var kelas = JSON.parse('{!! $tahun_ajaran->mapWithKeys(function ($item) { return [$item['id'] => $item];})->toJson() !!}');
        
        $('#id_tahun_angkatan').change(function() {
            $('#id_kelas').html('<option value="">Pilih Kelas</option>');
            if (kelas[$(this).val()].kelas != undefined) {
                $.each(kelas[$(this).val()].kelas, function(index, item) {
                    $('#id_kelas').append('<option value="'+item.id+'">'+item.nama+'</option>');
                });   
            }
        });
    });
</script>
@endsection
