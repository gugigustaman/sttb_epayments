@extends('layouts.app')

@section('title', 'Daftar User')

@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/node_modules/datatables/datatables.min.css"/>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar User
                        <a href="/admin/user/create" class="btn btn-sm btn-info pull-right"><i class="fa fa-plus"></i> &nbsp; Tambah</a>
                    </h4>
                    <h6 class="card-subtitle">User yang terdaftar</code></h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table" id="index">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Level</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/assets/node_modules/datatables/datatables.min.js"></script>
<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#index').DataTable({
            // processing: true,
            serverSide: false,
            ajax: {
                url: '{!! url('/admin/user/dtIndex') !!}',
                type: 'POST'
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'username', name: 'username' },
                { data: 'nama', name: 'nama' },
                { data: 'level', name: 'level', render: function(data, type, row) {
                    return data == 1 ? 'Administrator' : data == 2 ? 'Keuangan' : 'Unknown';
                }},
                { data: 'id', name: 'id', className: 'text-center actions' ,
                    render: function ( data, type, row ) {
                        var actions = "<a href='/admin/user/"+data+"/edit' class='btn btn-warning btn-xs'><i class='fa fa-edit'></i></a>";
                        actions += " <button data-id='"+data+"' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i></button>";
                        return actions;
                    }
                }
            ]
        });

        $('body').on('click', '.delete', function() {
            $('#id').val($(this).attr('data-id'));
            $('#delete-form').attr('action', '/admin/user/' + $('#id').val());
            swal({
                title: "Apakah Anda yakin?",
                text: "User tersebut akan dihapus dari sistem.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!!",
                closeOnConfirm: false
            },
            function(){
                $('#delete-form').submit();
            });
        });
    });
</script>
@endsection
