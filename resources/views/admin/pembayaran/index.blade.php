@extends('layouts.app')

@section('title', 'Daftar Pembayaran')

@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/node_modules/datatables/datatables.min.css"/>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Pembayaran
                        <!-- <a href="/admin/pembayaran/create" class="btn btn-sm btn-info pull-right"><i class="fa fa-plus"></i> &nbsp; Tambah</a> -->
                    </h4>
                    <h6 class="card-subtitle">Pembayaran yang berhasil</code></h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table" id="index">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Semester</th>
                                    <th>Nominal</th>
                                    <!-- <th>Aksi</th> -->
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/assets/node_modules/datatables/datatables.min.js"></script>
<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#index').DataTable({
            // processing: true,
            serverSide: false,
            ajax: {
                url: '{!! url('/admin/pembayaran/dtIndex') !!}',
                type: 'POST'
            },
            aaSorting: [[0,'desc']],
            columns: [
                { data: 'updated_at', name: 'updated_at' },
                { data: 'mahasiswa.npm', name: 'mahasiswa.npm' },
                { data: 'mahasiswa.nama', name: 'mahasiswa.nama' },
                { data: 'semester', name: 'semester', className: 'text-center'},
                { data: 'nominal', name: 'nominal', className: 'text-right'},
                // { data: 'id', name: 'id', className: 'text-center actions' ,
                //     render: function ( data, type, row ) {
                //         var actions = "<a href='/admin/pembayaran/"+data+"/edit' class='btn btn-warning btn-xs'><i class='fa fa-edit'></i></a>";
                //         actions += " <button data-id='"+data+"' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i></button>";
                //         return actions;
                //     }
                // }
            ]
        });
    });
</script>
@endsection
