@extends('mahasiswa.layout.app')

@section('title', 'Bayar')

@section('styles')
<link href="/css/pages/dashboard1.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Start Notification -->
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <!-- Tab panes -->
                <div class="card-body">
                    <h4 class="card-title">Bayar</h4>
                    <h6 class="card-subtitle">{{ $biaya->nama }}</h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <form method="POST" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <!-- <div class="form-group">
                            <label class="col-md-12">NPM</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" disabled value="{{ Auth::user()->npm }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Nama</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" disabled value="{{ Auth::user()->nama }}">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-12">Pilih Semester</label>
                            <div class="col-sm-12">
                                <select name="semester" id="semester" class="form-control form-control-line" required>
                                    @foreach ($smstr as $s)
                                    @if (Auth::user()->pembayaran_semester($s->semester)->sum('nominal') < $biaya->biaya_kelas_x(Auth::user()->id_kelas)->nominal)
                                    <option
                                    @if ($semester == $s->semester)
                                     selected
                                    @endif
                                    >{{ $s->semester }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="width: 50%">
                                <div class="form-group">
                                    <label class="col-md-12">Total</label>
                                    <div class="col-md-12">
                                        <input type="number" class="form-control form-control-line" disabled value="{{ $total }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="width: 50%">
                                <div class="form-group">
                                    <label class="col-md-12">Terbayar</label>
                                    <div class="col-md-12">
                                        <input type="number" class="form-control form-control-line" disabled value="{{ $terbayar }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Lunasi atau Cicil?</label>
                            <div class="col-sm-12">
                                <select name="jenis" id="jenis" class="form-control form-control-line" required>
                                    <option value="1" selected>Lunasi</option>
                                    <option value="2">Cicil</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Masukkan Nominal Pembayaran</label>
                            <div class="col-md-12">
                                <input name="nominal" type="number" min="0" max="{{ $sisa }}" class="form-control" id="nominal" form-control-line" value="{{ $sisa }}" readonly required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success" style="width: 100%">Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Notification -->
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        if ($('#semester').val() != {{ $semester}}) {
            window.location.href = '{{ url('/bayar') }}/'+$('#semester').val();
        }

        $('#semester').change(function() {
            window.location.href = '{{ url('/bayar') }}/'+$(this).val();
        });

        $('#jenis').change(function() {
            console.log($(this).val());
            if ($(this).val() == 1) {
                $('#nominal').prop('readonly', true);
                $('#nominal').val({{ $sisa }});
            } else if ($(this).val() == 2) {
                $('#nominal').prop('readonly', false);
                $('#nominal').val({{ $sisa }});
            }
        })
    });
</script>
@endsection
