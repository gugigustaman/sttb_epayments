@extends('mahasiswa.layout.app')

@section('title', 'Histori Pembayaran')

@section('styles')
<style type="text/css">
	table td {
		vertical-align: middle !important;
	}

	table td.nominal {
		font-size: 10px;
	}

	table td .dibayar {
		font-size: 14px;
		color: #000;
	}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Histori</h4>
                    <h6 class="card-subtitle">Daftar Pembayaran</code></h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <th>Semester</th>
                                    <th>Jumlah</th>
                                    <!-- <th>Aksi</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($pembayaran as $bayar)
                                <tr>
                                    <td>{{ $bayar->updated_at }}</td>
                                    <td align="center">{{ $bayar->semester }}</td>
                                    <td>Rp. {{ number_format($bayar->nominal, 0, ',', '.') }}</td>
                                    <!-- <td><button class="btn btn-xs btn-info">Lihat</button></td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection

@section('scripts')

@endsection