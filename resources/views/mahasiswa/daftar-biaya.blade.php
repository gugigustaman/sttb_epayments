@extends('mahasiswa.layout.app')

@section('title', 'Daftar Biaya')

@section('styles')
<style type="text/css">
	table td {
		vertical-align: middle !important;
	}

	table td.nominal {
		font-size: 10px;
	}

	table td .dibayar {
		font-size: 14px;
		color: #000;
	}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Biaya</h4>
                    <h6 class="card-subtitle">{{ $biaya->nama }}</code></h6>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Semester</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach ($semester as $s)
                                <tr>
                                    <td align="center">#{{ $s->semester }}</td>
                                    <td class="nominal"><span class="dibayar">Rp. {{ number_format(Auth::user()->pembayaran_semester($s->semester)->sum('nominal'), 0, ',', '.') }}</span><br />
                                    / Rp. {{ number_format($biaya->biaya_kelas_x(Auth::user()->id_kelas)->nominal, 0, ',', '.') }}
                                    </td>
                                    <td>
                                    	@if (Auth::user()->pembayaran_semester($s->semester)->sum('nominal') < $biaya->biaya_kelas_x(Auth::user()->id_kelas)->nominal)
                                    	<a href="/bayar/{{ $s->semester }}" class="btn btn-xs btn-primary">Bayar</a>
                                    	@else
                                    	Lunas
                                    	@endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection

@section('scripts')

@endsection