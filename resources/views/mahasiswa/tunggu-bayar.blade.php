@extends('mahasiswa.layout.app')

@section('title', 'Menunggu Pembayaran')

@section('styles')
<link rel="stylesheet" href="/css/jquery.growl.css">
<style type="text/css">
    .va {
        margin-top: 20px;
        text-align: center;
    }
    .va p {
        color: #000;
        font-size: 18px;
        margin-bottom: 0px;
    }
    .cancel {
        margin-top: 30px;
        text-align: center;
    }
    .cancel a {
        color: red;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Start Notification -->
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class=" card-title">Menunggu pembayaran</h5>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <!-- <span class="label label-info label-rounded">Technology</span> -->
                    <p class="m-b-0 m-t-20">Halo, {{ Auth::user()->nama }}. Silakan lakukan pembayaran dengan detail berikut.</p>
                    <div class="va">
                        <label>Nama Bank</label>
                        <p>Bukopin</p>
                    </div>
                    <div class="va">
                        <label>No. Virtual Account</label>
                        <p>{{ $tunggu->kode_va }}</p>
                        <a href="#" class="copy" data-text="{{ $tunggu->kode_va }}">SALIN</a>
                    </div>
                    <div class="va">
                        <label>Nominal</label>
                        <p>Rp. {{ number_format($tunggu->nominal, 0, ',', '.') }}</p>
                        <a href="#" class="copy" data-text="{{ $tunggu->nominal }}">SALIN</a>
                    </div>
                    <div class="va">
                        <label>Kadaluarsa pada</label>
                        <p>{{ $tunggu->waktu_kadaluarsa }}</p>
                    </div>
                    <div class="cancel">
                        <a href="#" onclick="event.preventDefault(); document.getElementById('form_batal').submit();">Batalkan</a>
                        <form id="form_batal" action="{{ url('/bayar/batal') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Notification -->
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/jquery.growl.js"></script>
<script type="text/javascript">
    $(function() {
        window.Clipboard = (function(window, document, navigator) {
            var textArea,
                copy;

            function isOS() {
                return navigator.userAgent.match(/ipad|iphone/i);
            }

            function createTextArea(text) {
                textArea = document.createElement('textArea');
                textArea.value = text;
                document.body.appendChild(textArea);
            }

            function selectText() {
                var range,
                    selection;

                if (isOS()) {
                    range = document.createRange();
                    range.selectNodeContents(textArea);
                    selection = window.getSelection();
                    selection.removeAllRanges();
                    selection.addRange(range);
                    textArea.setSelectionRange(0, 999999);
                } else {
                    textArea.select();
                }
            }

            function copyToClipboard() {        
                document.execCommand('copy');
                document.body.removeChild(textArea);
            }

            copy = function(text) {
                createTextArea(text);
                selectText();
                copyToClipboard();
            };

            return {
                copy: copy
            };
        })(window, document, navigator);

        $('.copy').click(function() {
            Clipboard.copy($(this).data('text'));
            $.growl({ 
                location: 'bc',
                title: 'Sukses',
                message: 'Teks berhasil disalin' 
            });
        });
    });
</script>
@endsection
