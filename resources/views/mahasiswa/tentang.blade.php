@extends('mahasiswa.layout.app')

@section('title', 'Tentang')

@section('styles')
<link rel="stylesheet" href="/css/jquery.growl.css">
<style type="text/css">
    .va {
        margin-top: 20px;
        text-align: center;
    }
    .va p {
        color: #000;
        font-size: 18px;
        margin-bottom: 0px;
    }
    .cancel {
        margin-top: 30px;
        text-align: center;
    }
    .cancel a {
        color: red;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Start Notification -->
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h5 class=" card-title">Tentang Aplikasi</h5> -->
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <img src="/assets/images/logo_sttb.jpg" class="img-responsive" />
                    <div class="va">
                        <label>Aplikasi</label>
                        <p>STTB e-Payments</p>
                    </div>
                    <div class="va">
                        <label>Versi</label>
                        <p>1.0</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Notification -->
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/jquery.growl.js"></script>
<script type="text/javascript">
    $(function() {
        window.Clipboard = (function(window, document, navigator) {
            var textArea,
                copy;

            function isOS() {
                return navigator.userAgent.match(/ipad|iphone/i);
            }

            function createTextArea(text) {
                textArea = document.createElement('textArea');
                textArea.value = text;
                document.body.appendChild(textArea);
            }

            function selectText() {
                var range,
                    selection;

                if (isOS()) {
                    range = document.createRange();
                    range.selectNodeContents(textArea);
                    selection = window.getSelection();
                    selection.removeAllRanges();
                    selection.addRange(range);
                    textArea.setSelectionRange(0, 999999);
                } else {
                    textArea.select();
                }
            }

            function copyToClipboard() {        
                document.execCommand('copy');
                document.body.removeChild(textArea);
            }

            copy = function(text) {
                createTextArea(text);
                selectText();
                copyToClipboard();
            };

            return {
                copy: copy
            };
        })(window, document, navigator);

        $('.copy').click(function() {
            Clipboard.copy($(this).data('text'));
            $.growl({ 
                location: 'bc',
                title: 'Sukses',
                message: 'Teks berhasil disalin' 
            });
        });
    });
</script>
@endsection
