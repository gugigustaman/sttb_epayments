@extends('mahasiswa.layout.app')

@section('title', 'Beranda')

@section('styles')
<link href="/css/pages/dashboard1.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Start Notification -->
        <div class="col-lg-12 col-md-12">
            <div class="card card-body mailbox">
                <h5 class="card-title">Menu Utama</h5>
                @if (session('message'))
                    <div class="alert alert-{{ session('type') }}">
                    {!! session('message') !!}
                    </div>
                @endif
                <div class="message-center ps ps--theme_default ps--active-y" data-ps-id="a045fe3c-cb6e-028e-3a70-8d6ff0d7f6bd" style="height: auto !important">
                    <!-- Message -->
                    <a href="/biaya">
                        <div class="btn btn-danger btn-circle"><i class="fa fa-list"></i></div>
                        <div class="mail-contnet">
                            <h5>Daftar Biaya</h5> <span class="mail-desc">Daftar biaya kuliah</span></div>
                    </a>
                    <!-- Message -->
                    <a href="/bayar/1">
                        <div class="btn btn-info btn-circle"><i class="fa fa-money"></i></div>
                        <div class="mail-contnet">
                            <h5>Bayar</h5> <span class="mail-desc">Bayar biaya terdekat</span></div>
                    </a>
                    <!-- Message -->
                    <a href="/histori">
                        <div class="btn btn-success btn-circle"><i class="fa fa-file-o"></i></div>
                        <div class="mail-contnet">
                            <h5>Histori</h5> <span class="mail-desc">Lihat Histori Pembayaran</span></div>
                    </a>
                </div>
            </div>
        </div>
        <!-- End Notification -->
    </div>
</div>
@endsection

@section('scripts')
<!-- <script src="/js/dashboard1.js"></script> -->
@endsection
