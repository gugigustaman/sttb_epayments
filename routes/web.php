<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'simulasi'], function() {
	Route::get('/masuk', 'SimulasiController@masuk');
	Route::post('/masuk', 'SimulasiController@doMasuk');
	Route::get('/transfer', 'SimulasiController@transfer');
	Route::post('/transfer', 'SimulasiController@doTransfer');
	Route::get('/nominal', 'SimulasiController@nominal');
	Route::post('/nominal', 'SimulasiController@doNominal');
	Route::get('/konfirmasi', 'SimulasiController@konfirmasi');
	Route::post('/konfirmasi', 'SimulasiController@doKonfirmasi');
	Route::get('/keluar', 'SimulasiController@keluar');
	Route::get('/berhasil', 'SimulasiController@berhasil');
});

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'HomeController@homeMhs')->name('home');
	Route::get('/biaya', 'BiayaController@daftar')->name('daftar_biaya');
	Route::get('/bayar/tunggu', 'PembayaranController@tunggu_bayar')->name('bayar.tunggu');
	Route::post('/bayar/batal', 'PembayaranController@batal_bayar')->name('bayar.batal');
	Route::get('/bayar/{semester}', 'PembayaranController@bayar')->name('bayar');
	Route::post('/bayar/{semester}', 'PembayaranController@doBayar')->name('bayar.do');
	Route::get('/histori', 'PembayaranController@histori');
	Route::get('/tentang', 'HomeController@tentang');
});

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');
});
