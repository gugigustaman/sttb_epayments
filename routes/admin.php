<?php

Route::get('/', 'HomeController@index')->name('home');
Route::resource('mahasiswa', 'MahasiswaController');
Route::post('/mahasiswa/dtIndex', 'MahasiswaController@dtIndex');
Route::resource('jurusan', 'JurusanController');
Route::post('/jurusan/dtIndex', 'JurusanController@dtIndex');
Route::resource('tahunangkatan', 'TahunAngkatanController');
Route::post('/tahunangkatan/dtIndex', 'TahunAngkatanController@dtIndex');
Route::resource('kelas', 'KelasController');
Route::post('/kelas/dtIndex', 'KelasController@dtIndex');
Route::resource('biaya', 'BiayaController');
Route::post('/biaya/dtIndex', 'BiayaController@dtIndex');
Route::resource('biayakelas', 'BiayaKelasController');
Route::post('/biayakelas/dtIndex', 'BiayaKelasController@dtIndex');
Route::resource('user', 'UserController');
Route::post('/user/dtIndex', 'UserController@dtIndex');
Route::resource('pembayaran', 'PembayaranController');
Route::post('/pembayaran/dtIndex', 'PembayaranController@dtIndex');
