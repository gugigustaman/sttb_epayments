<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelas extends Model
{
	use SoftDeletes;

	protected $table = 'kelas';
	protected $primaryKey = 'id';

	protected $fillable = [
		'nama', 'id_tahun_ajaran'
	];

	protected $dates = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function biaya() {
		return $this->hasMany('App\BiayaKelas', 'id_kelas', 'id');
	}

	public function tahun_ajaran() {
		return $this->belongsTo('App\TahunAjaran', 'id_tahun_ajaran', 'id');
	}
}
