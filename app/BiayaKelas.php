<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BiayaKelas extends Model
{
	use SoftDeletes;

    protected $table = 'biaya_kelas';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'id_biaya', 'id_kelas', 'nominal'
    ];

    protected $dates = [
    	'created_at', 'updated_at', 'deleted_at'
    ];

    public function biaya() {
    	return $this->belongsTo('App\Biaya', 'id_biaya', 'id');
    }

    public function kelas() {
    	return $this->belongsTo('App\Kelas', 'id_kelas', 'id');
    }
}
