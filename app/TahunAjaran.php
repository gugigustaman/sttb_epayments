<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TahunAjaran extends Model
{
	use SoftDeletes;

	protected $table = 'tahun_ajaran';
	protected $primaryKey = 'id';

	protected $fillable = [
		'tahun_ajaran',
	];

	protected $dates = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function kelas() {
		return $this->hasMany('App\Kelas', 'id_tahun_ajaran', 'id');
	}
}
