<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Semester extends Model
{
	use SoftDeletes;

	protected $table = 'semester';
	protected $primaryKey = 'id';

	protected $fillable = [
		'semester', 'wajib'
	];

	protected $dates = [
		'created_at', 'updated_at', 'deleted_at'
	];
}
