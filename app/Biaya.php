<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biaya extends Model
{
	use SoftDeletes;

    protected $table = 'biaya';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'nama',
    ];

    protected $dates = [
    	'created_at', 'updated_at', 'deleted_at'
    ];

    public function biaya_kelas() {
    	return $this->hasMany('App\BiayaKelas', 'id_biaya', 'id');
    }

    public function biaya_kelas_x($id_kelas) {
    	return $this->biaya_kelas()->where('id_kelas', $id_kelas)->first();
    }
}
