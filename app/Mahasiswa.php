<?php

namespace App;

use App\Notifications\MahasiswaResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mahasiswa extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'mahasiswa';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'npm', 'nama', 'email', 'password', 'id_jurusan', 'id_kelas', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MahasiswaResetPassword($token));
    }

    public function jurusann() {
        return $this->belongsTo('App\Jurusan', 'id_jurusan', 'id');
    }

    public function kelas() {
        return $this->belongsTo('App\Kelas', 'id_kelas', 'id');
    }

    public function pembayaran() {
        return $this->hasMany('App\Pembayaran', 'npm', 'npm');
    }

    public function pembayaran_semester($semester) {
        return $this->pembayaran()->where('semester', $semester)->where('status', 1)->get();
    }

    public function tunggu_pembayaran() {
        return $this->pembayaran()->where('status', 0)->first();
    }
}
