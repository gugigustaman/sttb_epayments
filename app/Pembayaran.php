<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Pembayaran extends Model
{
    use SoftDeletes;

    protected $table = 'pembayaran';
    protected $primaryKey = 'id';
    public static $va_prefix = '770378';

    protected $fillable = [
    	'npm', 'id_biaya_kelas', 'semester', 'nominal', 'kode_unik', 'status', 'waktu_kadaluarsa'
    ];

    protected $dates = [
    	'created_at', 'updated_at', 'deleted_at'
    ];

    public function biaya_kelas() {
    	return $this->hasMany('App\BiayaKelas', 'id_biaya_kelas', 'id');
    }

    public function semester() {
    	return $this->belongsTo('App\Semester', 'semester', 'semester');
    }

    public function mahasiswa() {
    	return $this->belongsto('App\Mahasiswa', 'npm', 'npm');
    }

    public static function generateVa($npm) {
    	$width = 10;
    	$padded = str_pad((string)$npm, $width, "0", STR_PAD_LEFT); 
    	return Pembayaran::$va_prefix . $padded;
    }

    public function isPending() {
    	return $this->status == 0;
    }

    public function isExpired() {
		if ($this->status == 3) {

		}

    	$now = Carbon::now();
    	$expired = Carbon::parse($this->waktu_kadaluarsa);

    	// Jika saat ini sudah melebihi tanggal expired
    	if ($now->gt($expired)) {
    		$this->status = 3;
    		$this->save();
    		return true;
    	}

    	return false;
    }

    public function batal() {
    	$this->status = 3;
    	if (!$this->save()) {
    		return false;
    	}

    	return true;
    }
}
