<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Pembayaran;

class SimulasiController extends Controller
{
    public function masuk() {
    	return view('simulasi.masuk');
    }

    public function doMasuk(Request $request) {
    	if (!$request->has('pin')) {
    		return redirect('/simulasi/masuk')
    			->with('type', 'danger')
    			->with('message', 'Anda harus memasukkan PIN terlebih dahulu');
    	}

    	if ($request->pin != '000000') {
    		return redirect('/simulasi/masuk')
    			->with('type', 'danger')
    			->with('message', 'PIN yang Anda masukkan salah');
    	}

    	Session::put('simulasi_token', md5(Carbon::now()->format('YmdHis')));

    	return redirect('/simulasi/transfer');
    }

    public function transfer(Request $request) {

    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}
    	return view('simulasi.transfer');
    }

    public function doTransfer(Request $request) {
    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}
    	
    	if (!$request->has('kode_va')) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Silakan masukkan nomor Virtual Account terlebih dahulu.');
    	}

    	$bayar = Pembayaran::where('kode_va', $request->kode_va)->orderBy('created_at', 'desc')->first();

    	if ($bayar == null) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Akun tidak terdaftar. Silakan periksa kembali No. Virtual Account yang Anda masukkan.');
    	}

    	Session::put('simulasi_va', $request->kode_va);

    	return redirect('/simulasi/nominal');
    }

    public function nominal(Request $request) {
    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}

    	return view('simulasi.nominal');
    }

    public function doNominal(Request $request) {
    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}

    	if (!$request->has('nominal')) {
    		return redirect('/simulasi/nominal')
    			->with('type', 'danger')
    			->with('message', 'Silakan masukkan nominal terlebih dahulu.');
    	}

    	if ($request->nominal <= 0) {
    		return redirect('/simulasi/nominal')
    			->with('type', 'danger')
    			->with('message', 'Nominal harus lebih dari Rp. 0,-.');
    	}

    	Session::put('simulasi_nominal', $request->nominal);

    	return redirect('/simulasi/konfirmasi');
    }

    public function konfirmasi(Request $request) {
    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}

    	$kode_va = Session::get('simulasi_va');
    	$nominal = Session::get('simulasi_nominal');

    	$bayar = Pembayaran::where('kode_va', $kode_va)->orderBy('created_at', 'desc')->first();

    	if ($bayar == null) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Akun tidak terdaftar. Silakan periksa kembali No. Virtual Account yang Anda masukkan.');
    	}

    	$bayar->nominal = $nominal;

    	return view('simulasi.konfirmasi', [
    		'bayar' => $bayar
    	]);
    }

    public function doKonfirmasi(Request $request) {
    	if (!Session::has('simulasi_token')) {
    		return redirect('/simulasi/masuk');
    	}

    	$kode_va = Session::get('simulasi_va');
    	$nominal = Session::get('simulasi_nominal');

    	$bayar = Pembayaran::where('kode_va', $kode_va)->orderBy('created_at', 'desc')->first();

    	if ($bayar == null) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Akun tidak terdaftar. Silakan periksa kembali No. Virtual Account yang Anda masukkan.');
    	}

    	if (!$bayar->isPending() || $bayar->isExpired()) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Pembayaran sudah tidak valid.');
    	}

    	$bayar->nominal = $nominal;
    	$bayar->status = 1;

    	if (!$bayar->save()) {
    		return redirect('/simulasi/transfer')
    			->with('type', 'danger')
    			->with('message', 'Terjadi kesalahan pada sistem. Mohon ulangi beberapa saat lagi.');
    	}

    	return redirect('/simulasi/berhasil');
    }

    public function berhasil() {
    	return view('simulasi.berhasil');
    }

    public function keluar() {
    	Session::forget('simulasi_token');
    	return redirect('/simulasi/masuk');
    }
}
