<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'npm' => 'required|string|unique:user,npm',
            'nama' => 'required|string',
            'password' => 'required|string',
            'password2' => 'required|string|same:password',
            'email' => 'required|email',
            'id_jurusan' => 'required|string|exists:jurusan,id',
            'id_kelas' => 'required|string|exists:kelas,id',
            'status' => 'required|numeric|in:0,1'
        ]);

        if ($validator->fails()) {
            dd($request->all(), $validator->errors());
            return redirect('/admin/user/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Silakan periksa kembali formulir berikut. Pastikan semua kolom terisi dengan benar.');
        }

        $user = new User();
        $user->npm = $request->npm;
        $user->nama = $request->nama;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->id_jurusan = $request->id_jurusan;
        $user->id_kelas = $request->id_kelas;
        $user->status = $request->status;

        if (!$user->save()) {
            return redirect('/admin/user/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/admin/user')
            ->with('type', 'success')
            ->with('message', 'User berhasil ditambahkan ke sistem.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dtIndex(Request $request) {
        return datatables()->of(User::query())->toJson();
    }
}
