<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biaya;
use App\BiayaKelas;
use App\Semester;
use App\Pembayaran;
use Auth;

class BiayaController extends Controller
{
	public function daftar(Request $request) {
		$semester = Semester::where('semester', '<=', 8)->get();
		$biaya = Biaya::find(1);
		$user = Auth::user();

		return view('mahasiswa.daftar-biaya', [
			'semester' => $semester,
			'biaya' => $biaya
		]);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.biaya.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.biaya.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'npm' => 'required|string|unique:biaya,npm',
            'nama' => 'required|string',
            'password' => 'required|string',
            'password2' => 'required|string|same:password',
            'email' => 'required|email',
            'id_jurusan' => 'required|string|exists:jurusan,id',
            'id_kelas' => 'required|string|exists:kelas,id',
            'status' => 'required|numeric|in:0,1'
        ]);

        if ($validator->fails()) {
            return redirect('/admin/biaya/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Silakan periksa kembali formulir berikut. Pastikan semua kolom terisi dengan benar.');
        }

        $biaya = new Biaya();
        $biaya->npm = $request->npm;
        $biaya->nama = $request->nama;
        $biaya->password = Hash::make($request->password);
        $biaya->email = $request->email;
        $biaya->id_jurusan = $request->id_jurusan;
        $biaya->id_kelas = $request->id_kelas;
        $biaya->status = $request->status;

        if (!$biaya->save()) {
            return redirect('/admin/biaya/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/admin/biaya')
            ->with('type', 'success')
            ->with('message', 'Biaya berhasil ditambahkan ke sistem.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dtIndex(Request $request) {
        return datatables()->of(Biaya::query())->toJson();
    }
}