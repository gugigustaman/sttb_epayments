<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Biaya;
use Auth;
use App\Semester;
use App\Pembayaran;
use Carbon\Carbon;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pembayaran.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jurusan = Jurusan::all();
        $tahun_ajaran = TahunAjaran::with('kelas')->get();
        return view('admin.pembayaran.create', [
            'jurusan' => $jurusan,
            'tahun_ajaran' => $tahun_ajaran
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'npm' => 'required|string|unique:pembayaran,npm',
            'nama' => 'required|string',
            'password' => 'required|string',
            'password2' => 'required|string|same:password',
            'email' => 'required|email',
            'id_jurusan' => 'required|string|exists:jurusan,id',
            'id_kelas' => 'required|string|exists:kelas,id',
            'status' => 'required|numeric|in:0,1'
        ]);

        if ($validator->fails()) {
            dd($request->all(), $validator->errors());
            return redirect('/admin/pembayaran/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Silakan periksa kembali formulir berikut. Pastikan semua kolom terisi dengan benar.');
        }

        $pembayaran = new Pembayaran();
        $pembayaran->npm = $request->npm;
        $pembayaran->nama = $request->nama;
        $pembayaran->password = Hash::make($request->password);
        $pembayaran->email = $request->email;
        $pembayaran->id_jurusan = $request->id_jurusan;
        $pembayaran->id_kelas = $request->id_kelas;
        $pembayaran->status = $request->status;

        if (!$pembayaran->save()) {
            return redirect('/admin/pembayaran/create')
                ->withInput()
                ->withErrors($validator)
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/admin/pembayaran')
            ->with('type', 'success')
            ->with('message', 'Pembayaran berhasil ditambahkan ke sistem.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dtIndex(Request $request) {
        return datatables()->of(Pembayaran::with('mahasiswa')->where('status', 1))->toJson();
    }

    public function bayar(Request $request, $semester = 1) {
    	$s = Semester::where('semester', $semester)->first();

    	$smstr = Semester::where('semester', '<=', '8')->get();
    	$biaya = Biaya::find(1);

    	$mhs = Auth::user();

    	if ($mhs->tunggu_pembayaran() != null) {
    		return redirect('/bayar/tunggu')
    			->with('type', 'danger')
    			->with('message', 'Anda masih mempunyai pembayaran yang belum Anda bayar. Silakan bayar terlebih dahulu atau batalkan untuk melakukan pembayaran lain.');
    	}

    	$total = $biaya->biaya_kelas_x(Auth::user()->id_kelas)->nominal;
    	$terbayar = Auth::user()->pembayaran_semester($semester)->sum('nominal');
    	$sisa = $total - $terbayar;
    	
    	return view('mahasiswa.bayar', [
    		'semester' => $semester,
    		'smstr' => $smstr,
    		'biaya' => $biaya,
    		'total' => $total,
    		'terbayar' => $terbayar,
    		'sisa' => $sisa
    	]);
    }

    public function doBayar(Request $request, $semester) {
    	$s = Semester::where('semester', $semester)->first();

    	if ($s == null) {
    		return redirect('/')
    			->with('type', 'danger')
    			->with('message', 'Semester tidak ditemukan');
    	}

    	$mhs = Auth::user();

    	if ($mhs->tunggu_pembayaran() != null) {
    		return redirect('/bayar/tunggu')
    			->with('type', 'danger')
    			->with('message', 'Anda masih mempunyai pembayaran yang belum Anda bayar. Silakan bayar terlebih dahulu atau batalkan untuk melakukan pembayaran lain.');
    	}

    	$biaya = Biaya::find(1);
    	$total = $biaya->biaya_kelas_x(Auth::user()->id_kelas)->nominal;
    	$terbayar = Auth::user()->pembayaran_semester($semester)->sum('nominal');
    	$sisa = $total - $terbayar;
    	$smstr = Semester::where('semester', '<=', '8')->get();

    	$validator = Validator::make($request->all(), [
    		'nominal' => 'required|numeric|max:'.$sisa
    	]);

    	if ($validator->fails()) {
    		return redirect('/bayar/'.$semester)
    			->with('type', 'danger')
    			->with('message', 'Silakan isi dengan benar formulir pembayaran');
    	}

    	$pembayaran = new Pembayaran();
    	$pembayaran->npm = $mhs->npm;
    	$pembayaran->kode_va = Pembayaran::generateVa($mhs->npm);
    	$pembayaran->id_biaya_kelas = $biaya->biaya_kelas_x(Auth::user()->id_kelas)->id;
    	$pembayaran->semester = $semester;
    	$pembayaran->nominal = $request->nominal;
    	$pembayaran->status = 0;
    	$now = Carbon::now();
    	$pembayaran->waktu_kadaluarsa = $now->addHours(6);

    	if (!$pembayaran->save()) {
    		return redirect('/bayar/'.$semester)
    			->with('type', 'danger')
    			->with('message', 'Terjadi kesalahan pada sistem. Mohon ulangi beberapa saat lagi.');
    	}

    	return redirect('/bayar/tunggu')
    		->with('pembayaran', $pembayaran);
    }

    public function tunggu_bayar() {
    	$mhs = Auth::user();
    	$tunggu = $mhs->tunggu_pembayaran();

    	if ($tunggu == null) {
    		return redirect('/biaya');
    	}

    	return view('mahasiswa.tunggu-bayar', [
    		'tunggu' => $tunggu
    	]);
    }

    public function batal_bayar() {
    	$mhs = Auth::user();
    	$tunggu = $mhs->tunggu_pembayaran();

    	if ($tunggu == null) {
    		return redirect('/biaya');
    	}

    	if (!$tunggu->batal()) {
    		return redirect('/bayar/tunggu')
    			->with('type', 'danger')
    			->with('message', 'Telah terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
    	}

    	return redirect('/biaya')
    		->with('type', 'info')
    		->with('message', 'Pembayaran telah berhasil dibatalkan.');
    }

    public function histori() {
    	$pembayaran = Auth::user()->pembayaran()->where('status', 1)->orderBy('created_at', 'desc')->get();

    	return view('mahasiswa.histori', [
    		'pembayaran' => $pembayaran
    	]);
    }
}

