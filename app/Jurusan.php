<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jurusan extends Model
{
	use SoftDeletes;
	
    protected $table = 'jurusan';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
    	'nama'
    ];

    protected $dates = [
    	'created_at', 'updated_at', 'deleted_at'
    ];
}
