<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('npm');
            $table->string('kode_va');
            $table->unsignedInteger('id_biaya_kelas');
            $table->unsignedInteger('semester');
            $table->unsignedInteger('nominal');
            $table->unsignedInteger('status');
            $table->datetime('waktu_kadaluarsa');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('npm')->references('npm')->on('mahasiswa')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_biaya_kelas')->references('id')->on('biaya_kelas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('semester')->references('semester')->on('semester')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran');
    }
}
