<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiayaKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biaya_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_biaya');
            $table->unsignedInteger('id_kelas');
            $table->unsignedInteger('nominal');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_biaya')->references('id')->on('biaya')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_kelas')->references('id')->on('kelas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biaya_kelas');
    }
}
