<?php

use Illuminate\Database\Seeder;

class JurusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('jurusan')->insert([
	        [
	        	'id' => 'TI',
	        	'nama' => 'Teknik Industri',
	        	'created_at' => Carbon\Carbon::now(),
	        	'updated_at' => null,
	        	'deleted_at' => null,
	        ],
	        [
	        	'id' => 'IF',
	        	'nama' => 'Teknik Informatika',
	        	'created_at' => Carbon\Carbon::now(),
	        	'updated_at' => null,
	        	'deleted_at' => null,
	        ],
	        [
	        	'id' => 'DKV',
	        	'nama' => 'Desain Komunikasi dan Visual',
	        	'created_at' => Carbon\Carbon::now(),
	        	'updated_at' => null,
	        	'deleted_at' => null,
	        ],
        ]);
    }
}
