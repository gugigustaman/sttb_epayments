<?php

use Illuminate\Database\Seeder;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('semester')->insert([
        	[
        		'semester' => 1,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 2,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 3,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 4,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 5,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 6,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 7,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 8,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 9,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 10,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 11,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 12,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 13,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        	[
        		'semester' => 14,
        		'wajib' => 1,
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
        ]);
    }
}
