<?php

use Illuminate\Database\Seeder;

class BiayaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('biaya')->insert([
		    [
		    	'nama' => 'Biaya Kuliah Semester',
		    	'created_at' => Carbon\Carbon::now(),
		    	'updated_at' => null,
		    	'deleted_at' => null,
		    ],
		]);
    }
}
