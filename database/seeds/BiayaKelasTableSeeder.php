<?php

use Illuminate\Database\Seeder;

class BiayaKelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('biaya_kelas')->insert([
		    [
		    	'id_biaya' => 1,
		    	'id_kelas' => 1,
		    	'nominal' => 3000000,
		    	'created_at' => Carbon\Carbon::now(),
		    	'updated_at' => null,
		    	'deleted_at' => null,
		    ],
		    [
				'id_biaya' => 1,
				'id_kelas' => 2,
				'nominal' => 3150000,
				'created_at' => Carbon\Carbon::now(),
				'updated_at' => null,
				'deleted_at' => null,
			],
		    [
				'id_biaya' => 1,
				'id_kelas' => 3,
				'nominal' => 3350000,
				'created_at' => Carbon\Carbon::now(),
				'updated_at' => null,
				'deleted_at' => null,
			],
		    [
		    	'id_biaya' => 1,
		    	'id_kelas' => 4,
		    	'nominal' => 2750000,
		    	'created_at' => Carbon\Carbon::now(),
		    	'updated_at' => null,
		    	'deleted_at' => null,
		    ],
		    [
				'id_biaya' => 1,
				'id_kelas' => 5,
				'nominal' => 3000000,
				'created_at' => Carbon\Carbon::now(),
				'updated_at' => null,
				'deleted_at' => null,
			],
		    [
				'id_biaya' => 1,
				'id_kelas' => 6,
				'nominal' => 3500000,
				'created_at' => Carbon\Carbon::now(),
				'updated_at' => null,
				'deleted_at' => null,
			],
		]);
    }
}
