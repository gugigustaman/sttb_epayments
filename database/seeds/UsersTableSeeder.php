<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            	'nama' => 'Administrator',
            	'username' => 'admin',
            	'password' => bcrypt('rahasia'),
            	'level' => 1,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
            [
            	'nama' => 'Keuangan',
            	'username' => 'keuangan',
            	'password' => bcrypt('rahasia'),
            	'level' => 2,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
        ]);
    }
}
