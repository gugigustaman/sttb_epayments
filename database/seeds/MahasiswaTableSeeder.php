<?php

use Illuminate\Database\Seeder;

class MahasiswaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('mahasiswa')->insert([
	        [
	        	'npm' => '1541811',
	        	'nama' => 'Gugi Gustaman',
	        	'email' => 'gugi.gustaman.15@gmail.com',
	        	'password' => bcrypt('rahasia'),
	        	'id_jurusan' => 'IF',
	        	'id_kelas' => 2,
	        	'status' => 1,
	        	'created_at' => Carbon\Carbon::now(),
	        	'updated_at' => null,
	        	'deleted_at' => null,
	        ],
	        [
	        	'npm' => '1541307',
	        	'nama' => 'Kintan Tiara',
	        	'email' => 'kintan.tiara@gmail.com',
	        	'password' => bcrypt('rahasia'),
	        	'id_jurusan' => 'IF',
	        	'id_kelas' => 1,
	        	'status' => 1,
	        	'created_at' => Carbon\Carbon::now(),
	        	'updated_at' => null,
	        	'deleted_at' => null,
	        ]
        ]);
    }
}
