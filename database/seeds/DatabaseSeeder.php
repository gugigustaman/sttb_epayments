<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TahunAjaranTableSeeder::class);
        $this->call(KelasTableSeeder::class);
        $this->call(JurusanTableSeeder::class);
        $this->call(SemesterTableSeeder::class);
        $this->call(MahasiswaTableSeeder::class);
        $this->call(BiayaTableSeeder::class);
        $this->call(BiayaKelasTableSeeder::class);
    }
}
