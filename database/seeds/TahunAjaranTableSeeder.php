<?php

use Illuminate\Database\Seeder;

class TahunAjaranTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tahun_ajaran')->insert([
        	[
        		'tahun_ajaran' => '2015/2016',
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => null,
        		'deleted_at' => null,
        	],
            [
            	'tahun_ajaran' => '2016/2017',
            	'created_at' => Carbon\Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
            [
            	'tahun_ajaran' => '2017/2018',
            	'created_at' => Carbon\Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
        ]);
    }
}
