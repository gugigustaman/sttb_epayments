<?php

use Illuminate\Database\Seeder;

class KelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas')->insert([
            [
            	'nama' => 'Reguler Pagi',
            	'id_tahun_ajaran' => 1,
            	'created_at' => Carbon\Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
            [
            	'nama' => 'Reguler Malam',
            	'id_tahun_ajaran' => 1,
            	'created_at' => Carbon\Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
            [
            	'nama' => 'Karyawan',
            	'id_tahun_ajaran' => 1,
            	'created_at' => Carbon\Carbon::now(),
            	'updated_at' => null,
            	'deleted_at' => null,
            ],
        ]);
    }
}
